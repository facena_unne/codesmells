/* 
 *Ejemplos de code smells.
 *Grupo 6 - Teoria de la Computación
 *2021-25
 */

//Caso de ejemplo copia de la pseudo variabl this.

var self = this;
var nombre_variable   =   this   ;
let nombrevariable =   this;
let NOMBREvariable = this;
let nombreVariable   = this;

//Fin del ejemplo:- Caso de ejemplo copia de la pseudo variabl this.
