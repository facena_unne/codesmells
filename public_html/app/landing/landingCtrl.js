/** * 
 *  Controlador  de Angular 
 *  UTF-8 28/11/2020 
 *  landing
 *  Pablo N. Garcia Solanellas
 *  Corrientes, Argentina.
 **/

controllers.controller('LandingCtrl',
['$scope', 'AssetsFactory', 'ModelFactory','ServiceFactory',
    '$routeParams', '$filter', 'AnalyticsFactory',
    '$timeout',
function ($scope, AssetsFactory, ModelFactory,ServiceFactory,
    $routeParams, $filter, AnalyticsFactory,
    $timeout)
{
//VARIABLES PRIVADAS*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
    var _im = {
        caption: 'Landing',
        name: 'landing',
        requiredAssets: [],
        alreadyInitFlag: false,
        preinitInitFlag:false,
        pub: {}
    };
    var _defaults = {};
    _defaults[_im.name] = {};

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//VARIALBES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-        
    $scope.model={};
    $scope.inner = {
        data:{},
        ORDEN_BY: {
            DIRECTION: "-",
            COLUM: "id"
        }
    };
    $scope.knowSmellTypes={
        overMessage:{
            isSelected:true,            
            title:"Pase de múltiples mensajes sobre una misma referencia a un objeto",
            autorId:4,
            detalle:"En POO este caso se presenta cuando ocurre cuando se envían múltiples mensajes a una misma instancia.",
            ejemplo:[
                "objeto.metodo().metodo().metodo(3);"
            ],
            regEx: [
                /(([\w\d])(\.|))+([\w\d\s](\()(\))(\.))+([\w\d().])*/g,
                /(([\w\d])(\.|))+([\w\d](\()+[\w\d\s\,]*(\))(\.))+([\w\d](\()+[\w\d](|)+(\)))*.*/g
            ],            
            hlcls:"highlight-overMessage",
            singleLine:true,
            lines:[]
        },
        tooManyParammeters:{
            isSelected:true,
            title:"Demasiados parámetros en un mensaje",
            autorId:2,
            detalle:"Esto constituye un mal uso de la POO. Se deben remplazar los parametros por un objeto.",
            ejemplo:[
                "objeto.metodo(a,b,c,d,e,f,j);\n"
            ],
            regEx: [/\((\w+(,)){3,}(\w+)\)/g],
            hlcls:"highlight-tooManyParammeters",
            singleLine:true,
            lines:[]
        },
        copyThis:{
            isSelected:true,
            title:"Copia de instancia",
            autorId:1,
            detalle:"Las instancias no debe copiarse de esta manera.",
            ejemploLink: "assets/ejemplos/copia-this.js",
            ejemplo:[
                "var self = this;",
                "var nombre_variable   =   this   ;",
                "let nombre.variable   =   this;",
                "let NOMBREvariable   =   this;",
                "let nombreVariable   =   this;"
            ],
            regEx:[
                /(var|let)(\s*)(.*)(\s*)=(\s*)this(\s*);/
            ] ,
            hlcls:"highlight-copy-this",
            singleLine:true,
            lines:[]
        },
        returnThis:{
            isSelected:true,
            title:"Retorno de la pseudo variable this.",
            autorId:4,
            detalle:"No es recomendable que un objeto se retorna a sí mismo a por medio de la pseudovariable this. se recomienda el uso de un patron como singleton para este fin.",
            ejemplo:[
                "return this;"
            ],
            regEx: [/return(\s)(this);/g],
            hlcls:"highlight-return-this",
            singleLine:true,
            lines:[]
        },
        concatenacionDeString:{
            isSelected:true,
            title:"Concatenación de cadenas de manera incremental",
            autorId:3,
            detalle:"El uso del operador '+=' para la  concatenación de cadenas de texto puede producir resultados indeseados si el orden de ejecución de las sentencias cambia",
            ejemplo:[
                "if (!teroriaDeLaComputacion){ \n texto += 'solo '\n }\n texto += 'queremos ';\n texto += 'aprobar ';\n texto += 'la  ';\n texto += 'materia ';\n texto += 'firma ';\n texto += 'Fety ';"
            ],
            regEx: [/(\+= "(\w+|\d+[.]\d+)";|(\+=)\s+(.*);)|(\+="(\w+|\d+[.]\d+)";)|\+= "/g],
            hlcls:"highlight-concatenar-string",
            singleLine:true,
            lines:[]
        },
        ifInnerCase:{
            isSelected:true,
            title:"If dentro de Case",
            autorId:2,
            detalle:"aumenta la complejidad del software Ver: metrica de McCabe.",
            ejemplo:[
                "case: \nif(algo){\n}"
            ],
            link:"https://ieeexplore.ieee.org/document/1702388",
            regEx: [/case:(\s)if[(](\w+|\d+[.]\d+)([<]|[>]|[=]|[<=]|[>=])(\w+|\d+[.]\d+)[)]|[{][}]/g],
            hlcls:"highlight-if-inner-case",
            singleLine:true,
            lines:[]
        },
        IfAnidados:{
            isSelected:true,
            title:"If anidados",
            autorId:4,
            detalle:"Aumenta la complejidad del software Ver: metrica de McCabe.",
            ejemplo:[
                "If (){If (){ } } "
            ],
            link:"https://ieeexplore.ieee.org/document/1702388",
            regEx: [/^(\if\s*\()[^\r\n]+([\w\d\s\;\\/\=\\+]|\})*/gm],
            hlcls:"highlight-if-anidado",
            singleLine:false,
            lines:[]
        },
        returnHardcore:{
            isSelected:true,
            title:"Retorno de valor booleano de manera literal",
            autorId:2,
            detalle:"En POO este caso se presenta cuando ocurre cuando se envían múltiples mensajes a una misma instancia.",
            ejemplo:[
                "objeto.mensage().mensage2().mensage(3)..."
            ],
            link:"https://ntgard.medium.com/returning-boolean-literals-is-a-code-smell-7a39531d6b60",
            regEx: [/return(\s)((true)|(false))[;]/g],
            hlcls:"highlight-return-hard-core",
            singleLine:true,
            lines:[]
        },
        emtyCatch:{
            isSelected:true,
            title:"Pobre manejo de excepciones",
            autorId:3,
            detalle:"Las expresiones atrapadas en tiempo de ejecución que no se manejan adecuadamente pueden ocultar errores y por lo tanto dificultar la depuración.",
            ejemplo:[
                "catch(){ }\n catch(e){\n }"
            ],
            regEx: [
                /(\s*catch\s*\()[\w\s]*[\W]+(\)[^\w][\W]|\})/gm,
                /(catch\s*\()[^\w][\W]*(\)|\})/gm
            ],
            hlcls:"highlight-emty-catch-1",
            singleLine:true,
            lines:[]
        },
        setSeveralVarsInSameStatement:{
            isSelected:true,
            title:"Inicialización de múltiples variables en una sola sentencia",
            autorId:3,
            detalle:"Mala practica",
            ejemplo:[
                "var a, b, c, d, e = null;"
            ],
            regEx: [
                /var((.*,){1})(\w+|\d+[.]\d+)=/gm
            ],
            hlcls:"highlight-several-vars-in-same-line",
            singleLine:true,
            lines:[]
        },
        IfenUnaLinea:{
            isSelected:true,
            title:"If en una sola linea",
            autorId:4,
            detalle:"Mala practica",
            ejemplo:[
                " isTrue?'algiuna accion':'alguna otra';"
            ],
            regEx: [                
                /(.*)?(.*):(.*);/g
            ],
            hlcls:"highlight-if-anidado",
            singleLine:true,
            lines:[]
        }
        
        
    };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES $scope*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    $scope.swLandingView = function (p_index, p_data) {
        $scope[_im.name].view = $scope[_im.name].views.list[p_index];
        $scope.main.view = $scope[_im.name].view;
        if (typeof p_data !== "undefined")
        {
            $scope.model[$scope[_im.name].view.name] = angular.copy(p_data);
            //if ($scope[_im.name].view.updatePath){$location.update_path( $filter('locationIdFilter')($scope[_im.name].view, p_data)); }       
        } else {
            $scope.model[$scope[_im.name].view.name] = angular.copy(_defaults[$scope[_im.name].view.name]);
        }
        //$scope.broadCastManager($scope[_im.name].view.eventName);
        //iniciar select2
        if ($scope[_im.name].view.hasOwnProperty("smartSelect")) {
            angular.forEach($scope[_im.name].view.smartSelect, function (_object) {
                var _selectId = Object.keys(_object)[0];
                _initSelect2(_selectId, _object[_selectId]);
            });
        }
        /*if (_im.alreadyInitFlag) {
         $location.update_path( $filter('locationIdFilter')($scope[_im.name].view, p_data));
         }*/
    };
    $scope.getSourceCode = function () 
    {        
        var _innerOnSuccess=function(p_response){
            if (p_response){
                $scope.model.external.codigoFuente = p_response;
                $scope.model.external.lines = [];
                $scope.model.external.lines = $scope.model.external.codigoFuente.split("\n");
                $timeout(_olfatearCodigo,100);    
            }else{
                $scope.msgbox("warning", "URL", "No encontramos el codigo fuente que solicito.");
            }
            
        };
        ServiceFactory.get($scope.model.external.urlCodigoFuente,{}
        ).then(_innerOnSuccess, _innerOnSuccess);
    };
    
    $scope.scrolToLine=function(p_line)
    {        
        var t = $scope.model[$scope[_im.name].view.name].editor.charCoords({line: p_line, ch: 0}, "local").top; 
        var middleHeight = $scope.model[$scope[_im.name].view.name].editor.getScrollerElement().offsetHeight / 2; 
        $scope.model[$scope[_im.name].view.name].editor.scrollTo(null, t - middleHeight - 5); 
    };
    $scope.displayType=function()
    {
        var _innerReload=function(){
            $scope.model.external.codigoFuente = _cFuente;         
            _olfatearCodigo();     
        };
        
        let _cFuente = angular.copy($scope.model.external.codigoFuente);
        $scope.model.external.codigoFuente = "";
        
        $timeout(_innerReload,100);       
    };
    $scope.explorar=function(p_smeelKey){
        let _data = angular.copy(_defaults.explorer);
        _data.codeSmell = $scope.model.external.detecciones[p_smeelKey];
        _data.codigoEjemplo = $filter("inLineCode")(_data.codeSmell.ejemplo);     
        $scope.swLandingView($scope[_im.name].views.keys.explorer, _data);
    };
    $scope.paginarCoinsidencias=function(p_next)
    {        
        let _moveTo = 0;
        if (p_next)
        {
            _moveTo = ++$scope.model.external.globaMath.currrentLineIndex;   
        }else
        {
            _moveTo = --$scope.model.external.globaMath.currrentLineIndex;            
            if (_moveTo<0){_moveTo=0;}
        }
        
        $scope.scrolToLine($scope.model.external.globaMath.lines[_moveTo]);
    };
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-    
//FUNCRIONES privadas*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    var _olfatearCodigo=function()
    {   
        
        $scope.model.external.editor = document.querySelector('.CodeMirror').CodeMirror;
        $scope.model.external.detecciones = angular.copy($scope.knowSmellTypes);
        $scope.model.external.globaMath.currrentLineIndex=0;
        $scope.model.external.globaMath = angular.copy(_defaults.external.globaMath);
        angular.forEach($scope.model[$scope[_im.name].view.name].detecciones,function(_smellType, _key){
            if (_smellType.singleLine){
                angular.forEach($scope.model.external.lines,function(_line,_index){                     
                    angular.forEach(_smellType.regEx,function(_regEx){                        
                        if (_smellType.lines.indexOf(_index)===-1){
                            
                            //AQUÍ OCURRE LA MAGIA!!!!
                            if (_regEx.test(_line) && _smellType.isSelected){
                                if ($scope.model.external.globaMath.lines.indexOf(_index)===-1){
                                    $scope.model.external.globaMath.lines.push(_index);    
                                }                                
                                _smellType.lines.push(_index); 
                                //marcamos linea de interes.
                                $scope.model.external.editor.markText(
                                    {line: _index, ch: 0}, {line: _index++, ch: 1000}, 
                                    {className: _smellType.hlcls, title:_smellType.title}
                                );
                            }
                        }
                    });
                });
            }
        });        
         $scope.model.external.globaMath.lines.sortNumbers();
        $scope.model.external.reporte= $filter("buildReporte")($scope.model.external);
       $scope.msgbox(); 
       $timeout(function(){
           $scope.scrolToLine(1);
       },500);
    };   
    var _onLoadModel = function (p_model) {
        angular.forEach(p_model.models, function (_element, _key) {
            _defaults[_key] = p_model.models[_key];            
            $scope.model[_key] = angular.copy(_defaults[_key]);
        });
        _im.requiredAssets = p_model.config.requiredAssets;
        $scope.inner.data = p_model.data;
        $scope[_im.name] = p_model.pub;
        $scope.model.external.detecciones = angular.copy($scope.knowSmellTypes);
        AssetsFactory.refresh(_im.requiredAssets, $scope.assets).then(_init);
    };
//LAST FUNCTIONS init | preInit-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    var _preInit = function () {
        if ($scope.statics.hasOwnProperty('auth') && !_im.preinitInitFlag) {
            _im.preinitInitFlag = true;
            ModelFactory.get(_im.name, 60, "days").then(_onLoadModel);
        }
    };
    var _init = function () {
        if (AssetsFactory.areReady($scope.assets, _im.requiredAssets)) {            
            AnalyticsFactory.view("landing");
            _im.alreadyInitFlag = true;
            if (!isNaN(parseInt($routeParams.id)))
            {
                $scope.edit(parseInt($routeParams.id));
            } else {
                $scope.swLandingView($scope[_im.name].views.keys.external);
                $scope.msgbox();
            }
        }
    };    
    $scope.msgbox('espere', _im.caption, 'Cargando...');
    _preInit();
//$scope.$on("SOME_EVENT",_init);
    $scope.$on("LOAD_STATIC_ASSETS_COMPLETE", _preInit);
    


}]);



filters.filter('buildReporte',
['$filter',
   function($filter){    
    var pub =function( p_external ) {                            
        var _reporte ={
            totalDetecciones:0,            
        };
        angular.forEach(p_external.detecciones,function(_type){
            _reporte.totalDetecciones+= _type.lines.length;
        });
        
        return _reporte;
        
    };
    return pub;
}]);

filters.filter('inLineCode',
['$filter',
   function($filter){    
    var pub =function( p_ejemplos ) {                            
        let _code = "";
        angular.forEach(p_ejemplos,function(_line){
            _code +=_line+"\n";
        });
        return _code;
    };
    return pub;
}]);