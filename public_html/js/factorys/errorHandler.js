ngApp.factory('ErrorHandlerFactory', [function () {
    
    var pub={};
    
    pub.handler=function(p_errData){
        var _response={
            clss:'danger',
            title:'Error',
            text:p_errData.message
        };
        
        if (!p_errData.hasOwnProperty("error"))
        {//warning
            _response.clss="warning";
        }else
        {//Errores
            if(p_errData.error===3){
                _response.clss="danger";                
            }
        }
        return _response;
    };
        
    return pub;
}]);