/**
 * 
 * Envia datos a las propiedades Google Analytics 4 (GA4). Oct 2020.
 * IMPORTANTE: ES ligeramente distiento a Universal Analytics.
 * @requires: [Global site tag (gtag.js) - Google Analytics] en el index.html
 * @type recoleccitor de informacio.
 * @doc: https://developers.google.com/analytics/devguides/collection/ga4?hl=es
 */

//Globals
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
//.end Globals

ngApp.factory('AnalyticsFactory', [function () {
        
    var pub = {};    
    pub.catalog={};
    /**
     * En caso de reconoser al usuario asocia el id de usuario al comportamiento.
     * @param {type} p_user
     * @returns {undefined}
     */
    pub.setUserId=function(p_user){        
        gtag('set', 'userId', p_user);                     
    };
    
    /**
     * Notifica de un canvio en la vista.
     * @param {String} p_screnName: es el nombre de la pantalla que quieres asociar a la vista.
     * @returns {undefined}, 
     */
    pub.view=function(p_screenName){	        
        gtag('event', 'screen_view', 
            {'screen_name':p_screenName}
        );
    };   
    
    /**
     * Actualiza la pocicion de seguimiento y carga el catalogo de eventos disponible.
     * @param {String} p_screenName: es el nombre de la pantalla que quieres asociar a la vista.
     * @param {Object} p_viewCatalog: catalogo de eventos disponibles para utilizar en la vista <p_screenName>
     * @returns {undefined}
     */
    pub.initView=function(p_screenName, p_viewCatalog){	
        //enviamos la vista
        pub.view(p_screenName);
        //instanciamos el catalogo de eventos de la vista.
        pub.catalog=p_viewCatalog;
    };   
    /**
     * Un evento es una acción diferenciada que se produce en un momento 
     * concreto y que se asocia a un usuario de tu aplicación o de un 
     * sitio web; por ejemplo: {types: [page_view, add_to_cart o form_submit,etc]} .
     *      
     * @param {String} p_idEvent:<br/> identificador del evento 
     * IMPORTANTE:<br/> 
     *  1)El p_idEvent debe estar disponible (definido) en el catalogo.<br/>
     *  2)El id del evento es distino al nombre(name) por que el evento pude variar en attributos.<br/>    
     *  <br/>  
     * @param {Object} p_runTimeParametes: contiene los atributos del evento que se recolectaron en tiempo de ejecucion.  <br/>  
     * @doc: https://developers.google.com/analytics/devguides/collection/ga4/tag-guide <br/>
     * @returns {undefined}
     */
    pub.event=function(p_idEvent, p_runTimeParametes){             
        
        //hacemos una copia de la informacion del evento.
        let _event = angular.copy( pub.catalog[p_idEvent]);
        
        //verificamos si hay valores recolectados en tiempo 
        //de ejecucion para sobre escrivir. 
        if (typeof p_runTimeParametes !=="undefined"){
            //sobre escrivimos los valores recolectados en tiempo de ejecucion.
            angular.forEach(p_runTimeParametes,function(_value,_key){
                _event.data[_key] =_value;
            });
        }
        //enviamos el evento.
        gtag('event',_event.name, _event.data);
    }; 
    pub.init=function(p_GID, p_pagePath, p_pageTitle, p_callback){        
        //TODO:         
        gtag('config', p_GID, {
            'page_path': p_pagePath
        });
        
        if (typeof p_callback === 'function'){p_callback();}
    };
    return pub;
    
}]);