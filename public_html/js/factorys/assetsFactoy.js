/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ngApp.factory('AssetsFactory', 
['ObjectListFactory','localStorageService','ServiceFactory', '$q',
    function (ObjectListFactory,localStorageService, ServiceFactory, $q) {
    
    var pub={};
    //-PRIVATE
        
    //-public
    pub.areReady=function(p_assets, p_features, alreadyInitFlag){
        if(p_features.length===0)return true;
        if (!alreadyInitFlag){//si aun no se inicio
            var _ready=true;
            angular.forEach(p_features,function(_feature){
                if (p_assets.hasOwnProperty(_feature)){
                    _ready = _ready && p_assets[_feature].hasOwnProperty("list");
                }else{
                    _ready = _ready && false;
                }
            }); 
            return _ready;
        }else{
            return false;
        }
    };
    pub.isLatestPresent=function(p_assets, p_features){        
        return p_assets.hasOwnProperty(p_features);        
    };
    pub.updateStorage=function(p_element, p_assets, p_storageKeys){
        ObjectListFactory(p_element, p_assets);        
        localStorageService.set( p_storageKeys, p_assets );
        return true;
    };
    
    pub.refresh=function(p_assets, p_assetScope, p_service){
        
        if (p_assets.length>0){
            var _service= typeof p_service !=="undefined"?p_service : "services/assets/latest";
            var _assetsToRefresh = {};
            angular.forEach(p_assets,function(_key){
                var _inStorage = localStorageService.get( _key  );

                if ( 
                    _inStorage!== null && _inStorage.hasOwnProperty("latest"))
                {
                    _assetsToRefresh[_key] = _inStorage.latest;
                }
                else
                {
                    _assetsToRefresh[_key] = null;
                };
            });
            var _innerOnSucces=function( p_responce ){

                angular.forEach( p_responce, function( _data, _key ){                
                    if (_data.hasOwnProperty("latest")){                    
                        //Persist assets                
                        localStorageService.set( _key, _data );    
                        p_assetScope[_key]= _data;
                    }else{
                        //leemos la informacion guardada.
                        p_assetScope[_key]=localStorageService.get( _key);
                    }
                });
                return p_assetScope;
            };
            var _innerOnError=function( p_responce){return $q.reject(p_responce.data); };

            return ServiceFactory.get(_service, {assets:_assetsToRefresh} ).then(_innerOnSucces,_innerOnError);    
        }else{
             return $q.resolve({});
        }
    };
    return pub;
}]);