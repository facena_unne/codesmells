/**}
 * 
 *Importante requiere  compresor.js
 *
 */
ngApp.factory('CompressFactory', 
[   function ( ) {
    
    var pub={};
    
    pub.binaryLarge= function( p_blob, p_options, _resolve, _reject) {  
        
        return new Promise((_resolve, _reject) => {             
            new Compressor(p_blob, p_options);
        });
    };
        
    return pub;
}]);
