/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ngApp.factory('StorageFactory', 
    ['$scope','localStorageService', 'ServiceFactory','$timeout',
        function ($scope, localStorageService, ServiceFactory, $timeout) {
        
    var pub = {};
    var  _broadCastTrugger=function(p_eventName,p_arguments){
        $timeout(function(){
            $scope.$broadcast(p_eventName, p_arguments);        
        },100 );
    };
    pub.getAssetManager=function(p_service, p_storageKey, p_assetName, 
        p_callServerFucntion, p_onCompletEventName){
            
        var _innerSucces = function(response){
            if (response.hasOwnProperty("latest")){
                var _inStorage = localStorageService.get( $scope.storageKeys[p_storageKey] );
                if ( _inStorage !== null ){//CACHEADO                
                    if(_inStorage.latest < response.latest || _inStorage.hasOwnProperty("error"))
                    {
                        p_callServerFucntion( response.latest,p_storageKey, p_assetName, p_onCompletEventName);
                    }else{
                        
                        if (p_assetName.indexOf(".") !== -1){
                            var _arr= p_assetName.split(".");
                            $scope[_arr[0]][_arr[1]] = _inStorage;
                            _broadCastTrugger(p_onCompletEventName);         
                        }else{
                            $scope[p_assetName] = _inStorage;
                            _broadCastTrugger(p_onCompletEventName);     
                        }
                        
                    }
                }else{
                    p_callServerFucntion( response.latest,p_storageKey, p_assetName, p_onCompletEventName);
                }
            }else{
                $timeout(function(){
                    pub.getAssetManager(p_service,  p_storageKey, p_assetName, 
                        p_callServerFucntion, p_onCompletEventName);
                },1500);
            }
        };        
        ServiceFactory.get(p_service,null,true).then(_innerSucces);              
    };
        
    return pub;
    
}]);
