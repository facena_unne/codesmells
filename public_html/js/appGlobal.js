/* 
 * Module Design Pattern
 */
var AppGlobals=(function(){
    var pub={};
    pub.PROJECT_NAME="reSmeller";    
    pub.PROJECT_FOLDER_NAME= pub.PROJECT_NAME ;
    pub.COMMON_APP_URL = location.protocol+"//"+location.hostname;    
    pub.SERVER_PATH= pub.COMMON_APP_URL+":"+location.port+"/"+pub.PROJECT_FOLDER_NAME+"/"; 
    pub.APP_VERSION = "TIME_STAMP_JS_FILE_HERE";
    return pub;
})();

