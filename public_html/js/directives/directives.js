/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ngApp.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.on('change', onChangeHandler);
      element.on('$destroy', function() {
        element.off();
      });

    }
  };
});

ngApp.directive('codeEditor', function($timeout) {
    return {
        restrict: 'E',
        require: "?ngModel",
        replace: true,
        transclude: true,
        theme: 'base16-light',
        template: '<div class="code-editor"></div>',
        link: function(scope, element, attrs, ngModelCtrl, transclude) {
            scope.editor = CodeMirror( element[0], {
                mode:  "javascript",
                value: "function myScript()\n{\nreturn true;\n\n}\n",
                autoCloseBrackets: true,
                /*lineWrapping: true, parte las lineas para que se vean completas.*/
                lineNumbers: true,
                matchBrackets: true,                
                styleSelectedText: true,
                markText:(
                    {line: 1, ch: 1}, {line: 1, ch: 4}, {className: "styled-background"}
                ),
                extraKeys: {
                    "Enter": "newlineAndIndentContinueMarkdownList"
                }
            });
            
            if (ngModelCtrl) {
                ngModelCtrl.$render = function() {
                    scope.editor.setValue(ngModelCtrl.$viewValue);
                };
            }
            scope.editor.on('change', function() {
                ngModelCtrl.$setViewValue(scope.editor.getValue());
                scope.$emit('editor-change');
            });
        }
    }
});
